

Exercise 1 - Understanding the API
==================================

The am4ip library is composed of the following modules:

::

    am4ip
    │   datasets.py
    │   utils.py
    │   __init__.py
    │
    └───iqa
          correlation.py
          metrics.py
          __init__.py

The role of `datasets.py` is to prepare the data. This includes data loading, pre-processing,
batch preparation, conversion to channel-first implementation, and conversion to `torch.tensors`.

`utils.py` contains a set of tools and utility function that are helpful, but not mandatory to used.

`iqa`module is composed of two files. `metrics.py` which will contain all image quality metrics
to evaluate and `correlation.py` which contain two classes to computer Pearson Linear
Correlation Coefficient, and Spearman Rank-Correlation Coefficient.

Activity: Write a python script or a notebook that:

1. TID2013 dataset is loaded
2. N-MAE (Normalized mean absolute error) is computed for each distorted image
3. Compute Pearson LCC and Spearman RCC
4. Plot mean opinion score as a function of N-MAE
5. Conclude if whether or not N-MAE is good or not as an IQ metric overall, or for only few distortions.


Exercise 2 - Implement IQ Metrics
=================================
In `metrics.py`, implement IQ metrics presented during the course, and evaluate them
following Exercise 1 setup.

1. PSNR (both on gray level images and color images)
2. Total Variation
3. SSIM + Multi-scale version
4. LPIPS (with different backbone but all coefficients set to 1)
5. GMSD (the 3 different versions)
6. BRISQUE

Some advices for LPIPS:
To avoid re-implementing a deep network in order to access to intermediate,
outputs, you can register forward hooks (functions that are called after
the layer forward pass) as follows:

.. code:: python


   model = vgg16(weights=VGG16_Weights.IMAGENET1K_V1)
   outputs = []
   def vgg_hook(module, inp, out):
       outputs.append(out)

   model.features[4].register_forward_hook(vgg_hook)

After the forward pass of the layer with index 4, its output will be append
into the list `outputs`. Note that you have to clear this list after every
forward pass or it will keep the values from previous batch. Also, hooks
have to be registered only once and not at every forward pass.